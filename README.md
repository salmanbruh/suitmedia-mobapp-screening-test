# Suitmedia

Suitmedia Mobile Developer Intern - First Screening Test

## Screenshots

### Home Page

#### Initial State

<img src="README_images/homescreen_1.png" alt="Home Screen 1" style="zoom:25%;" />

#### Filling the name form field

<img src="README_images/homescreen_2.png" alt="Home Screen 1" style="zoom:25%;" />

### Pick Event and Pick Guest Screen

Accessed after clicking `Next` button on **Home Screen**.

#### Initial State

<img src="README_images/pickeventguest_1.png" alt="asd" style="zoom:25%;" />



### Event Screen

Accessed after click on the `Pilih Event` button on Pick Event and Pick Guest Screen. 

#### Initial State

*Apologies for the bad recording since this was recorded in an Emulator.

<img src="README_images/eventscreen_static.png" style="zoom:25%;" />

<img src="README_images/eventscreen_dynamic.gif" style="zoom:25%;" />

#### Clicking an Event

After a click of an event, the app will go back to the **Pick Event and Pick Guest Screen ** and change the `Pilih Event ` button label into the name of the event that you have chosen. In this case, we will pick the first event which is **wqhqo event** (the first one on the list). It should go back to the **Pick Event and Pick Guest Screen**  and change the label of the `Pilih event` button into `wqhqo event`.

<img src="README_images/pickeventguest_2.png" style="zoom:25%;" />

### Guest Screen

Can be accessed after clicking on the `Pilih Guest` button. Data taken from http://www.mocky.io/v2/596dec7f0f000023032b8017.

#### Initial state

<img src="README_images/guest.png" style="zoom:25%;" />

#### Clicking a Guest

After a click of a Guest, the app will go back to the **Pick Event and Pick Guest Screen**, change the label of the `Pilih guest` button to the name of the Guest that you picked, and show an alert/snackbar that returns a string based on the following rules:

- If the date of birthdate of the Guest is **divisible by 2**, returns `blackberry`
- If the date of birthdate of the Guest is **divisible by 3**, returns `android`
- If the date of birthdate of the Guest is **divisible by 2 AND 3**, returns `iOS`
- Else returns `Feature phone`

For example:

- Andi  has `birthdate = 2014-01-01`, should return `Feature phone`

  <img src="README_images/guest_andi.png" style="zoom:25%;" />

- Budi has `birthdate = 2000-02-02`, should return `blackberry`

  <img src="README_images/guest_budi.png" style="zoom:25%;" />

- Charlie has `birthdate = 2000-03-03`, should return `android`

  <img src="README_images/guest_charlie.png" style="zoom:25%;" />

- Dede has `birthdate = 2014-06-06`, should return `iOS`

  <img src="README_images/guest_dede.png" style="zoom:25%;" />

- Joko has `birthdate = 2014-02-12`, should return `iOS`

  <img src="README_images/guest_joko.png" style="zoom:25%;" />

