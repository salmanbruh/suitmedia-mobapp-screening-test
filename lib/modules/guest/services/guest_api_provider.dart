import 'dart:convert' as convert;

import 'package:http/http.dart';
import 'package:suitmedia_screening_test/modules/guest/models/guest.dart';

class GuestApiProvider {
  GuestApiProvider({this.client});

  final Client client;

  Future<List<Guest>> getGuests() async {
    Uri uri = Uri.https("www.mocky.io", "/v2/596dec7f0f000023032b8017");

    final response = await client.get(uri);

    var jsonResponse = convert.jsonDecode(response.body) as List<dynamic>;

    List<Guest> guestsList = [];
    jsonResponse.forEach((guestJson) {
      guestsList.add(Guest.fromJson(guestJson));
    });
    return guestsList;
  }
}
