import 'package:flutter/material.dart';
import 'package:get_it/get_it.dart';
import 'package:intl/intl.dart';
import 'package:suitmedia_screening_test/modules/guest/models/guest.dart';
import 'package:suitmedia_screening_test/modules/guest/services/guest_api_provider.dart';

class GuestScreen extends StatefulWidget {
  @override
  _GuestScreenState createState() => _GuestScreenState();
}

class _GuestScreenState extends State<GuestScreen> {
  GuestApiProvider _guestApiProvider = GetIt.I();
  List<Guest> _guestsList = [];

  @override
  void initState() {
    _getGuestsList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Center(
            child: Text("GUEST"),
          ),
        ),
        body: _guestsList.length == 0
            ? Center(child: CircularProgressIndicator())
            : _buildGuestGridView(context),
      ),
    );
  }

  Widget _buildGuestGridView(BuildContext context) {
    return GridView.count(
      crossAxisCount: 2,
      children: _guestsList.map((guest) => _buildGuestsCard(guest)).toList(),
    );
  }

  Widget _buildGuestsCard(Guest guest) {
    return InkWell(
      onTap: () {
        String alertMessage = _getAlertMessage(guest.birthdate);
        _buildSnackBar(context, Text(alertMessage));
        Navigator.pop(context, guest.name);
      },
      child: Container(
        margin: EdgeInsets.all(16),
        child: Center(
          child: Text(
            guest.name,
            style: TextStyle(
              color: Colors.black,
              fontSize: 20,
            ),
          ),
        ),
        decoration: BoxDecoration(
          image: DecorationImage(
            image: guest.image,
            fit: BoxFit.cover,
          ),
        ),
      ),
    );
  }

  ScaffoldFeatureController<SnackBar, SnackBarClosedReason> _buildSnackBar(
    BuildContext context,
    Widget content,
  ) {
    return ScaffoldMessenger.of(context)
        .showSnackBar(SnackBar(content: content));
  }

  String _getAlertMessage(DateTime birthDate) {
    int day = int.parse(DateFormat('dd').format(birthDate));

    if (day % 2 == 0) {
      if (day % 3 == 0) {
        return "iOS";
      } else {
        return "blackberry";
      }
    } else if (day % 3 == 0) {
      return "android";
    }

    return "Feature phone";
  }

  Future<void> _getGuestsList() async {
    List<Guest> guestsList = await _guestApiProvider.getGuests();
    _setGuestsList(guestsList);
  }

  void _setGuestsList(List<Guest> newGuestsList) {
    setState(() {
      _guestsList = newGuestsList;
    });
  }
}
