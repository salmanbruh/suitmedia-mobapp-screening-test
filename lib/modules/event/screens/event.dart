import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:suitmedia_screening_test/modules/event/models/event.dart';
import 'package:suitmedia_screening_test/services/random_datetime_generator.dart';
import 'package:suitmedia_screening_test/services/random_string_generator.dart';

class EventScreen extends StatefulWidget {
  @override
  _EventScreenState createState() => _EventScreenState();
}

class _EventScreenState extends State<EventScreen> {
  List<Event> _eventsList = [];

  @override
  void initState() {
    _eventsList = _getEventsList();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          automaticallyImplyLeading: false,
          title: Center(
            child: Text(
              "EVENT",
            ),
          ),
        ),
        body: ListView.separated(
          itemCount: _eventsList.length,
          itemBuilder: (BuildContext context, int index) {
            return Container(
              padding: EdgeInsets.symmetric(horizontal: 16, vertical: 8),
              child: _buildEventCard(context, _eventsList[index]),
            );
          },
          separatorBuilder: (BuildContext context, int index) => Divider(),
        ),
      ),
    );
  }

  List<Event> _getEventsList() {
    return List.generate(
      25,
      (index) => Event(
        name: generateRandomString(5) + " event",
        datetime: generateRandomDateTime(2000, 2050),
        image: Container(
          height: 75,
          child: Center(
            child: Icon(Icons.article),
          ),
        ),
      ),
    );
  }

  Widget _buildEventCard(BuildContext context, Event event) {
    return Card(
      child: InkWell(
        onTap: () {
          Navigator.pop(context, event.name);
        },
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Expanded(
              flex: 1,
              child: event.image,
            ),
            Expanded(
              flex: 3,
              child: Container(
                padding: const EdgeInsets.symmetric(horizontal: 8),
                margin: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(event.name),
                    SizedBox(height: 8),
                    Text(DateFormat('dd/MM/yy').format(event.datetime)),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
