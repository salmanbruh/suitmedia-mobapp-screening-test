import 'package:flutter/material.dart';

class Event {
  Event({this.name, this.datetime, this.image});

  String name;
  DateTime datetime;
  Widget image;
}
