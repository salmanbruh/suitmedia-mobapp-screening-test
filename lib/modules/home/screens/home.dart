import 'package:flutter/material.dart';
import 'package:suitmedia_screening_test/components/custom_button.dart';
import 'package:suitmedia_screening_test/components/custom_text_input_form_field.dart';
import 'package:suitmedia_screening_test/modules/home/screens/pick_event_guest.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  final TextEditingController _nameController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: Container(
          margin: EdgeInsets.symmetric(horizontal: 16),
          child: _buildInsertNameForm(context),
        ),
      ),
    );
  }

  Widget _buildInsertNameForm(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        _buildNameInputTextField(context),
        SizedBox(
          height: 16,
        ),
        _buildNextButton(context),
      ],
    );
  }

  Widget _buildNameInputTextField(BuildContext context) {
    return CustomTextInputFormField(
      label: "Masukkan nama...",
      controller: _nameController,
    );
  }

  Widget _buildNextButton(BuildContext context) {
    return Container(
      width: double.infinity,
      child: CustomButton(
        text: Text("Next"),
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => PickEventAndGuestScreen(
                insertedName: _nameController.text,
              ),
            ),
          );
        },
      ),
    );
  }
}
