import 'package:random_date/random_date.dart';

DateTime generateRandomDateTime(int startYear, int endYear) {
  return RandomDate.withRange(startYear, endYear).random();
}
