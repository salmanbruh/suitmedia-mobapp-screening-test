import 'package:get_it/get_it.dart';
import 'package:http/http.dart';
import 'package:suitmedia_screening_test/modules/guest/services/guest_api_provider.dart';

Future<void> getItInject() async {
  final client = Client();

  GetIt.I.registerSingleton<GuestApiProvider>(GuestApiProvider(client: client));
}
