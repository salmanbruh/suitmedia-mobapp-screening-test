import 'package:flutter/material.dart';

class CustomButton extends StatefulWidget {
  CustomButton({
    this.text,
    this.onPressed,
  });

  final Widget text;
  final VoidCallback onPressed;

  @override
  _CustomButtonState createState() => _CustomButtonState();
}

class _CustomButtonState extends State<CustomButton> {
  @override
  Widget build(BuildContext context) {
    return Material(
      child: OutlinedButton(
        child: widget.text,
        onPressed: widget.onPressed,
      ),
    );
  }
}
