import 'package:flutter/material.dart';

class CustomTextInputFormField extends StatelessWidget {
  CustomTextInputFormField({
    this.label,
    this.controller,
  });

  final String label;
  final TextEditingController controller;

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      decoration: InputDecoration(
        labelText: label,
      ),
    );
  }
}
